const express = require('express');

const mongoose = require('mongoose');

const connectDB = require('./config/db');
const cors = require('cors');
const app = express();


//connect db

app.use(cors());
connectDB();

app.use(express.json({
  extended:false
}));


//Define Routes
app.use('/',require('./routes/app'));
app.use('/api/url',require('./routes/url'));

const PORT = 3000;

app.listen(PORT, ()=>
  console.log(`server is running on port ${PORT}`));

// app.set('port', process.env.PORT || 3000);
// const server = app.listen(app.get('port'), () => {
//   console.log("Express running → PORT",server.address().port);
// });