const mongoose = require('mongoose');
const config = require('config');
const db =config.get('mongoURI');



const connectDB = async () =>{
    try {

        await mongoose.connect('mongodb://localhost:27017/urlshortner',{
            useNewUrlParser:true
        });
        console.log("mongodb connected...")
        
    } catch (error) {
        console.log(error);
        process.exit(1);
        
    }
}

module.exports = connectDB;
