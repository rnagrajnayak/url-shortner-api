const express = require('express');

const router = express.Router();

const Url = require('../models/url')


//get

router.get('/:code',async(req,res)=>{
    try {
        const url = await Url.findOne({urlCode:req.params.code});

        if(url){
            return res.redirect(url.longUrl);
        }
        else{
            res.status(404);
        }
        
    } catch (error) {
        res.status(500);
        
    }
});


module.exports = router;