const express = require('express');

const router = express.Router();
const validUrl = require('valid-url');
const shortId = require('shortid');
const config = require('config');


const Url = require('../models/url');


//post

router.post('/shorten', async (req,res) =>{
    const {longUrl } = req.body;
    const baseUrl = config.get('baseUrl');
       

     if(!validUrl.isUri(baseUrl)){
         res.send({
             status:401,
             message:'Please Enter Valid URL'
         })
     }


     const urlCode = shortId.generate();

     if(validUrl.isUri(longUrl)){

         try {
             let url = await Url.findOne({ longUrl })
             if(url) {
                
                 res.json(url);
             }else{
                 const shortUrl = baseUrl + '/' + urlCode;
                

                 url = new Url({
                     longUrl,
                     shortUrl,
                     urlCode,
                     date:new Date()
                 });
                 
                 await url.save();
                 res.sendStatus(200).json(url);
             }
         } catch (error) {
             res.status(500).json('server error');
             
         }
     }else{
         res.sendStatus(401).json('wrong url');
     }
})



module.exports = router;